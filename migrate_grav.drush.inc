<?php

/**
 * @file
 * Drush commands for the migrate_grav module.
 */

use Drush\Log\LogLevel;

/**
 * Implements hook_drush_command().
 */
function migrate_grav_drush_command() {
  return [
    'migrate-grav' => [
      'description' => 'Convert blog to Grav.',
    ],
  ];
}

function drush_migrate_grav() {
  \Drupal::service('migrate_grav.grav_migration')->run();
}

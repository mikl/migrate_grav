<?php

namespace Drupal\migrate_grav;

use Drupal\comment\CommentManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Path\AliasManagerInterface;
use Symfony\Component\Yaml\Dumper;

/**
 * Class GravMigrationService.
 *
 * @package Drupal\migrate_grav
 */
class GravMigrationService {

  /**
   * Core\Path\AliasManager definition.
   *
   * @var \Drupal\Core\Path\AliasManagerInterface
   */
  protected $aliasManager;

  /**
   * Drupal\comment\CommentManager definition.
   *
   * @var \Drupal\comment\CommentManagerInterface
   */
  protected $commentManager;

  /**
   * Drupal\Core\Entity\EntityTypeManager definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\node\NodeStorageInterface definition.
   *
   * @var \Drupal\node\NodeStorageInterface
   */
  protected $nodeStorage;

  /**
   * Constructor.
   */
  public function __construct(
    CommentManagerInterface $comment_manager,
    EntityTypeManagerInterface $entity_type_manager,
    AliasManagerInterface $alias_manager
  ) {
    $this->aliasManager = $alias_manager;
    $this->commentManager = $comment_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->nodeStorage = $entity_type_manager->getStorage('node');
  }

  /**
   * Convert nodes to post arrays, usable for saving.
   *
   * @param array $nodes Nodes to convert.
   *
   * @return array Blog post data.
   */
  protected function convertNodes(array $nodes) {
    $posts = [];

    foreach ($nodes as $node) {
      $post = [
        'meta' => [
          'title' => $node->title->value,
          'published' => ($node->status->value > 0) ? TRUE : FALSE,
          'drupal-id' => (integer) $node->id(),
          'date' => date('c', $node->getCreatedTime()),
          'routes' => [
            'aliases' => [
              '/node/' . $node->id(),
            ],
          ],
        ],
        'slug' => date('Y-m-d', $node->getCreatedTime()),
        'markdown' => $node->body->value,
      ];

      $alias = $this->aliasManager->getAliasByPath('/node/' . $node->id());

      if (!empty($alias)) {
        $post['meta']['routes']['default'] = $alias;
        $post['meta']['routes']['canonical'] = $alias;

        // Alias for old URLs with /blog.
        $post['meta']['routes']['aliases'][] = '/blog' . $alias;

        $parts = explode('/', $alias);
        $post['slug'] .= '-' . array_pop($parts);
      }

      $posts[] = $post;
    }

    return $posts;
  }

  /**
   * Run the migration.
   */
  public function run() {
    $nodes = $this->nodeStorage->loadByProperties(['type' => 'article']);

    $posts = $this->convertNodes($nodes);

    $this->writeFiles('blog', $posts);
  }

  /**
   * Write post files.
   *
   * @param string $base_path Folder to export to.
   * @param array $posts Array of posts.
   */
  protected function writeFiles($base_path, array $posts) {
    $dumper = new Dumper();

    foreach ($posts as $post) {
      $folder = $base_path . '/' . $post['slug'];
      file_prepare_directory($folder, FILE_CREATE_DIRECTORY);

      $contents = [
        '---', // YAML front matter start.
        $dumper->dump($post['meta'], 4, 4, TRUE),
        '---', // YAML front matter end.
        $post['markdown'],
        '', // To ensure the file ends with a linebreak.
      ];

      file_put_contents($folder . '/item.md', implode(PHP_EOL, $contents));

      drupal_set_message('Wrote blog post to folder: '. $folder);
    }
  }
}
